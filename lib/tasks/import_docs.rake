namespace :import_docs do
	desc "import the doctors from csv"
	task :data => :environment do
		require 'csv'
		CSV.foreach('C:\row\projects\mgprado_project\lib\tasks\docs.csv') do |row|
		name = row[0]
		active = row[1]
		Physician.create(name: name, active: active)
		end
	end
end