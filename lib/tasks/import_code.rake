namespace :import_code do
	desc "import the diagnostic codes from csv"
	task :data => :environment do
		require 'csv'
		CSV.foreach('C:\row\projects\scheduler\lib\tasks\CodeData.csv') do |row|
		name = row[0]
		code = row[1]
		cost = row[2].to_f
		Diagnostic.create(name: name, code: code, cost: cost)
		end
	end
end