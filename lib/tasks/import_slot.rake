namespace :import_slot do
	desc "import the intervals from csv"
	task :data => :environment do
		require 'csv'
		CSV.foreach('C:\row\projects\mgprado_project\lib\tasks\intervals.csv') do |row|
		name = row[0]
		start_time = row[1]
		end_time = row[2]
		Slot.create(name: name, start_time: start_time, end_time: end_time)
		end
	end
end