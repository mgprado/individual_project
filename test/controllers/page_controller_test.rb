require 'test_helper'

class PageControllerTest < ActionController::TestCase
  test "should get dashboard" do
    get :dashboard
    assert_response :success
  end

  test "should get patient_dash" do
    get :patient_dash
    assert_response :success
  end

  test "should get cancel" do
    get :cancel
    assert_response :success
  end

  test "should get physician_dash" do
    get :physician_dash
    assert_response :success
  end

  test "should get patient_hist" do
    get :patient_hist
    assert_response :success
  end

  test "should get physician_app" do
    get :physician_app
    assert_response :success
  end

  test "should get invoice" do
    get :invoice
    assert_response :success
  end

  test "should get admin_dash" do
    get :admin_dash
    assert_response :success
  end

end
