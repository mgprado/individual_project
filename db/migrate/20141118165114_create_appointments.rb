class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.date :appointment_date
      t.text :note
      t.boolean :cancelled
      t.integer :patient_id
      t.integer :physician_id
      t.integer :slot_id
      t.integer :diagnostic_id

      t.timestamps
    end
  end
end
