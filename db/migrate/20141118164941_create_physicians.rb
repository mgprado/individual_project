class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.string :name
      t.boolean :active

      t.timestamps
    end
  end
end
