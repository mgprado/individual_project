class CreateDiagnostics < ActiveRecord::Migration
  def change
    create_table :diagnostics do |t|
      t.string :name
      t.string :code
      t.decimal :cost

      t.timestamps
    end
  end
end
