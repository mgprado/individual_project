json.array!(@diagnostics) do |diagnostic|
  json.extract! diagnostic, :id, :name, :code, :cost
  json.url diagnostic_url(diagnostic, format: :json)
end
