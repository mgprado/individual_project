json.array!(@slots) do |slot|
  json.extract! slot, :id, :name, :start_time, :end_time
  json.url slot_url(slot, format: :json)
end
