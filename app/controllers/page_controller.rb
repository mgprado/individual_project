class PageController < ApplicationController
  def dashboard
    @appointments = Appointment.all
  end

  def patient_dash
  end

  def cancel
    @appointments = Appointment.all
  end

  def physician_dash
  end

  def patient_hist
    @physicians = Physician.all
    @appointments = Appointment.all
  end

  def physician_app
    @appointments = Appointment.all
  end

  def invoice
    @appointments = Appointment.all
  end

  def admin_dash
  end

  def physician_admin
    @physicians = Physician.all
  end

  
end