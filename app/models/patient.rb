class Patient < ActiveRecord::Base
	has_many :appointments
	has_many :slots, through: :appointments
	has_many :physicians, through: :appointments
end
