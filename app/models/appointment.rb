class Appointment < ActiveRecord::Base
	belongs_to :physician
	belongs_to :patient
	belongs_to :slot
	belongs_to :diagnostic

	validates_presence_of :appointment_date
	
end
