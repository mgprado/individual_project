class Slot < ActiveRecord::Base
	has_many :appointments
	has_many :physicians, through: :appointments
	has_many :patients, through: :appointments

	scope :avail, ->(s_name) { where("name != ?", s_name) }
	end
